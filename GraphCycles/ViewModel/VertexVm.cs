﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media.Imaging;
using GraphCycles.Common;

namespace GraphCycles.ViewModel
{
    public class VertexVm : ViewModelBase
    {
        public int Id { get; set; }
        public BitmapSource VertexIcon { get; set; }
        public ICollection<ConnectionVm> Connections { get; set; }
        public Vector Position { get; set; }
        public bool IsVisited { get; set; }
        public VertexVm()
        {
            Position = new Vector();
            Connections = new ObservableCollection<ConnectionVm>();
            IsVisited = false;
        }

        public override string ToString()
        {
            return Id.ToString()+": connections: "+Connections.Count;
        }
    }
}