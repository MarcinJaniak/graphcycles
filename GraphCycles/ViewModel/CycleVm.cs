﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using GraphCycles.Common;

namespace GraphCycles.ViewModel
{
    public class CycleVm : ViewModelBase
    {
        public string Name
        {
            get
            {
                string result = String.Empty;
                foreach (var item in Connections)
                {
                    result += item.Source.Id + "-" + item.Target.Id +"; ";
                }
                return result;
            }
        }

        public ICollection<ConnectionVm> Connections
        {
            get;
            set;
        }

        public CycleVm()
        {
            Connections = new ObservableCollection<ConnectionVm>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}