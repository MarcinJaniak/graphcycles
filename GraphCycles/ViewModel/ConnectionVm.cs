﻿using System;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Animation;
using GraphCycles.Common;

namespace GraphCycles.ViewModel
{
    public class ConnectionVm : ViewModelBase
    {
        public SolidColorBrush ConnectionColorBrush { get; set; }
        public VertexVm Target { get; set; }
        public VertexVm Source { get; set; }
        public double Weight { get; set; }
        

        public ConnectionVm(VertexVm source, VertexVm target)
        {
            Target = target;
            Source = source;

            Weight = Enumerable.Range(1, 100).OrderBy(x => Guid.NewGuid()).FirstOrDefault();

            ConnectionColorBrush = new SolidColorBrush(Colors.Red);
        }

        public override string ToString()
        {
            return Source.Id + "-" + Target.Id;
        }
    }
}