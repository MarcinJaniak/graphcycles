﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GraphCycles.Common;

namespace GraphCycles.ViewModel
{
    public class GraphVM : ViewModelBase
    {
        public ICollection<VertexVm> Verticles { get; set; }
        public ICollection<ConnectionVm> Connections { get; set; }

        public int ProbabilityOfCreatingConnections { get; set; }

        public int GeneratedVertexCount { get; set; }
        public int CyclesVertexCount { get; set; }

        public ICollection<CycleVm> Cycles { get; set; }

        public double Radius { get; set; }

        public ICommand GenerateGraphCommand { get; set; }

        public CycleVm SelectedCycle
        {
            get { return _selectedCycle; }
            set
            {
                foreach (var item in Connections)
                {
                    item.ConnectionColorBrush = new SolidColorBrush(Colors.Red);
                }
                _selectedCycle = value;



                foreach (var item in _selectedCycle.Connections)
                {
                    item.ConnectionColorBrush = new SolidColorBrush(Colors.Green);
                }

            }
        }
        private CycleVm _selectedCycle { get; set; }


        public GraphVM()
        {
            GenerateGraphCommand = new RelayCommand(x =>
            {
                GenerateGraph();
            });

            GeneratedVertexCount = 15;
            ProbabilityOfCreatingConnections = 100;
            Radius = 100;
            CyclesVertexCount = 4;
            GenerateGraph();
            Cycles = GetCycles(CyclesVertexCount);

        }

        private ObservableCollection<CycleVm> GetCycles(int vertexCount)
        {
            var result = new ObservableCollection<CycleVm>();


            var cycleStack = new ObservableCollection<ConnectionVm>();
            VertexVm initialVertex = Verticles.OrderBy(z => Guid.NewGuid()).FirstOrDefault();
            VertexVm randomVerticle = initialVertex;
            ConnectionVm randomConnection;
            while (true)
            {
                randomConnection = randomVerticle.Connections.OrderBy(z => Guid.NewGuid()).FirstOrDefault(x => !x.Target.IsVisited);
                if (randomConnection != null)
                {
                    cycleStack.Add(randomConnection);
                    randomVerticle = randomConnection.Target;
                    randomVerticle.IsVisited = true;
                }
                else
                {
                    cycleStack.Remove(randomConnection);
                    randomVerticle = cycleStack.Last().Source;
                }

                if (randomVerticle.Id == initialVertex.Id && cycleStack.Count > 1)
                {
                    break;
                }
            }

            if (cycleStack.Count == vertexCount)
            {
                result.Add(new CycleVm() { Connections = cycleStack });
            }


            //for (int i = 0; i < vertexCount; i++)
            //{

            //    var cycleToAdd = new CycleVm();
            //    var randomConn = Connections.OrderBy(z => Guid.NewGuid()).FirstOrDefault();

            //    cycleToAdd.Connections.Add(randomConn);
            //    randomConn = Connections.OrderBy(z => Guid.NewGuid()).FirstOrDefault();
            //    cycleToAdd.Connections.Add(randomConn);
            //    randomConn = Connections.OrderBy(z => Guid.NewGuid()).FirstOrDefault();
            //    cycleToAdd.Connections.Add(randomConn);
            //    randomConn = Connections.OrderBy(z => Guid.NewGuid()).FirstOrDefault();
            //    cycleToAdd.Connections.Add(randomConn);
            //    result.Add(cycleToAdd);
            //}


            return result;
        }

        private Vector GetPositionVector(int x0, int y0, double radius, int dividingPoints, int pointNo)
        {

            double angle = pointNo * (360 / dividingPoints);

            var x = (int)(x0 + radius * Math.Cos((Math.PI / 180) * angle));
            var y = (int)(y0 + radius * Math.Sin((Math.PI / 180) * angle));

            return new Vector(x, y);
        }


        private void GenerateGraph()
        {
            Connections = new ObservableCollection<ConnectionVm>();
            Verticles = new ObservableCollection<VertexVm>();

            Enumerable.Range(0, GeneratedVertexCount).Select(y =>
            {

                var newVertex = new VertexVm() { Id = y, Position = GetPositionVector(250, 250, Radius, GeneratedVertexCount, y) };

                if (y > 0)
                {
                    var randomVerticle = Verticles.OrderBy(z => Guid.NewGuid()).FirstOrDefault();

                    var connectionToAdd = new ConnectionVm(newVertex, randomVerticle);

                    newVertex.Connections.Add(connectionToAdd);

                    randomVerticle?.Connections.Add(connectionToAdd);

                    Connections.Add(connectionToAdd);
                }

                Verticles.Add(newVertex);

                return y;
            }).ToList();

            Verticles.Select(z =>
            {
                Verticles.Select(p =>
                {
                    if (Enumerable.Range(0, 100).OrderBy(g => Guid.NewGuid()).FirstOrDefault() <=
                           ProbabilityOfCreatingConnections)
                    {
                        if (!p.Connections.Any(c => c.Source.Id == z.Id || c.Target.Id == z.Id))
                        {
                            var connectionToAdd = new ConnectionVm(z, p);
                            z.Connections.Add(connectionToAdd);

                            p?.Connections.Add(connectionToAdd);
                            Connections.Add(connectionToAdd);
                        }
                    }
                    return p;
                }).ToList();
                //while (true)
                //{
                //    while (Enumerable.Range(0, 100).OrderBy(g => Guid.NewGuid()).FirstOrDefault() <=
                //           ProbabilityOfCreatingConnections)
                //    {

                //        var randomVerticle = Verticles.OrderBy(g => Guid.NewGuid()).FirstOrDefault();

                //        if (randomVerticle == null)
                //        {
                //            break;
                //        }

                //        if (!randomVerticle.Connections.Any(c => c.Source.Id == z.Id || c.Target.Id == z.Id))
                //        {
                //            z.Connections.Add(new ConnectionVm(z, randomVerticle));

                //            randomVerticle?.Connections.Add(new ConnectionVm(randomVerticle, z));
                //        }
                //        else
                //        {
                //            break;
                //        }
                //    }
                //    return z;

                //}
                return z;
            }).ToList();
        }
    }
}