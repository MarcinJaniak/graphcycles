﻿using System.ComponentModel;

namespace GraphCycles.Common
{
    public abstract class ViewModelBase:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
    }
}